const express = require('express')
const { MongoClient, ObjectId } = require('mongodb')
const bodyParser= require('body-parser')
const app = express()
const port = 3000
const client = new MongoClient("mongodb://localhost:27017/")
const MONGO_ID_REGEX = /^([0-9a-fA-F]{12}){1,2}$/

app.use(express.static('public'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))


app.get('/', (req, res) => {
  res.send('Hello World !')
})

app.get('/todos', async (req, res) => {
  try {
    await client.connect()
    const db = client.db("tododb")
    const todos = db.collection("todos")
    const todoList = await todos.find().toArray()  
    res.send(todoList)
  } catch(e) {  
    console.error(e)
  } finally {
    client.close()
  }  
})

app.get('/todos/:id', async (req, res) => {
  
  const { id } = req.params
  if  (!id.match(MONGO_ID_REGEX)){
    res.status(400).send("You shall not pass !");
  }
  try {
    await client.connect()
    const db = client.db("tododb")
    const todos = db.collection("todos")
    const todo = await todos.findOne({_id: new ObjectId(id)})
    
    res.send(todo)
  } catch(e) {  
    console.error(e)
  } finally {
    client.close()
  }  
})

app.post('/todo_create', async (req, res, next) => {

  if (!req.body.content)
  {
    res.status(400).send("You shall not pass !");
  } 

  try {  
    await client.connect()
    const db = client.db("tododb")
    const todos = db.collection("todos")    
    const result = await todos.insertOne(req.body)
    res.send(result.insertedId)      
  } catch(e) {  
    console.error(e)
  } finally {
    client.close()
  }
})


app.put('/todo/:id', async (req, res, next) => {

  const { id } = req.params
  if  (!id.match(MONGO_ID_REGEX)){
    res.status(400).send("You shall not pass !");
  }

  try {
    await client.connect()
    const db = client.db("tododb")
    const todos = db.collection("todos") 
  
    const result = await todos.findOneAndUpdate(
      {_id: req.params.id},
      {
        $set: {
          title: req.body.title,
          content: req.body.content
        }
      },
      {
        upsert: true,
        returnDocument: 'after'
      }
    )
    res.send(result)
  } catch(e) {  
    console.error(e)
  } finally {
    client.close()
  }

})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

app.use((error, req, res, next) => {
  return res.status(500).json({ error: 'Fly, you' });
});